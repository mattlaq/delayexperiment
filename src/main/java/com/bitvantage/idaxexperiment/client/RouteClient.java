/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author Public Transit Analytics
 */
public class RouteClient {

    private static final String TEMPLATE_URL
            = "/idax/v1/%s/routes/%s";
    private static final String KEY_HEADER = "X-API-KEY";

    private final String baseUrl;
    private final String key;
    private final OkHttpClient client;
    private final Gson serializer;

    public RouteClient(final String baseUrl, final String key) {
        this.baseUrl = baseUrl;
        this.key = key;
        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();
        serializer = new Gson();
    }

    public RouteResults getAllDescriptions()
            throws ClientException {
        return getDescriptions("*");
    }

    public RouteResults getDescriptions(final List<String> ids)
            throws ClientException {
        final StringJoiner joiner = new StringJoiner(",");
        ids.stream().forEach(joiner::add);
        final String descriptionString = joiner.toString();
        return getDescriptions(descriptionString);
    }

    private RouteResults getDescriptions(final String descriptionString)
            throws ClientException {

        final String url = baseUrl + String.format(
                           TEMPLATE_URL, "all", descriptionString);

        final Request request = new Request.Builder().addHeader(KEY_HEADER, key)
                .url(url).build();

        try {
            final Response response = client.newCall(request).execute();

            final String responseString = response.body().string();

            final RouteResults result = serializer.fromJson(
                    responseString, RouteResults.class);

            return result;
        } catch (final IOException e) {
            throw new ClientException(e);
        }

    }

}
