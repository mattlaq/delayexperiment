/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import lombok.Value;

/**
 *
 * @author Public Transit Analytics
 */
@Value
public class TimeBaselineResult {

    private final String name;
    private final String id;
    private final int period;
    private final double travel_time;
    private final double travel_dist_ratio;

}
