/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author Public Transit Analytics
 */
public class BaselineClient {

    private final Type TYPE = TypeToken.getParameterized(
            List.class, TimeBaselineResult.class).getType();
    private static final String TEMPLATE_URL
            = "/idax/v1/%s/baseline";
    private static final String KEY_HEADER = "X-API-KEY";

    private final String baseUrl;
    private final String key;
    private final OkHttpClient client;
    private final Gson serializer;

    public BaselineClient(final String baseUrl, final String key) {
        this.baseUrl = baseUrl;
        this.key = key;
        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();
        serializer = new Gson();
    }

    public List<TimeBaselineResult> getTravelTimes() throws ClientException {
        return getBaseline("gp_traveltimes");

    }

    public List<TimeBaselineResult> getTransitTravelTimes() throws ClientException {
        return getBaseline("bus_traveltimes");

    }

    private List<TimeBaselineResult> getBaseline(final String dataType)
            throws ClientException {
        final String url = baseUrl + String.format(TEMPLATE_URL, dataType);

        final Request request = new Request.Builder().addHeader(KEY_HEADER, key)
                .url(url).build();

        try {
            final Response response = client.newCall(request).execute();

            final String responseString = response.body().string();

            final List<TimeBaselineResult> result = serializer.fromJson(
                    responseString, TYPE);

            return result;
        } catch (final IOException e) {
            throw new ClientException(e);
        }
    }

}
