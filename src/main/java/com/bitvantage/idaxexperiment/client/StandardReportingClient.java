/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import com.google.gson.Gson;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author Public Transit Analytics
 */
@RequiredArgsConstructor
public class StandardReportingClient {

    private static final String TEMPLATE_URL
            = "/idax/v1/allroutes/%s/interval/%s/start/%d/end/%d";
    private static final String KEY_HEADER = "X-API-KEY";

    private final String baseUrl;
    private final String key;
    private final OkHttpClient client;
    private final Gson serializer;

    public StandardReportingClient(final String baseUrl, final String key) {
        this.baseUrl = baseUrl;
        this.key = key;
        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();
        serializer = new Gson();
    }

    public ReportingResults getVolume(
            final int interval, final LocalDateTime start,
            final LocalDateTime end) throws ClientException {
        return getStandardReporting("volumes", interval, start, end);

    }

    public ReportingResults getTravelTimes(
            final int interval, final LocalDateTime start,
            final LocalDateTime end) throws ClientException {
        return getStandardReporting("gp_traveltimes", interval, start, end);
    }

    public ReportingResults getTransitTravelTimes(
            final int interval, final LocalDateTime start,
            final LocalDateTime end) throws ClientException {
        return getStandardReporting("bus_traveltimes", interval, start, end);
    }

    public ReportingResults getEvents(
            final int interval, final LocalDateTime start,
            final LocalDateTime end) throws ClientException {
        return getStandardReporting("events", interval, start, end);
    }

    private ReportingResults getStandardReporting(
            final String dataType, final int interval,
            final LocalDateTime start, final LocalDateTime end)
            throws ClientException {
        final String url = baseUrl + String.format(
                           TEMPLATE_URL, dataType, interval,
                           start.toEpochSecond(ZoneOffset.UTC),
                           end.toEpochSecond(ZoneOffset.UTC));

        final Request request = new Request.Builder().addHeader(KEY_HEADER, key)
                .url(url).build();

        try {
            final Response response = client.newCall(request).execute();

            final String responseString = response.body().string();

            final ReportingResults result = serializer.fromJson(
                    responseString, ReportingResults.class);

            return result;
        } catch (final IOException e) {
            throw new ClientException(e);
        }
    }

}
