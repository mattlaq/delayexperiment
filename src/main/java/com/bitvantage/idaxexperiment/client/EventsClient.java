/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import com.google.gson.Gson;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author Public Transit Analytics
 */
public class EventsClient {

    private static final String TEMPLATE_URL
            = "/idax/v1/events/%s/start/%d/end/%d";
    private static final String KEY_HEADER = "X-API-KEY";

    private final String baseUrl;
    private final String key;
    private final ZoneOffset timezone;
    private final OkHttpClient client;
    private final Gson serializer;

    public EventsClient(final String baseUrl, final String key,
                        final ZoneOffset timezone) {
        this.baseUrl = baseUrl;
        this.key = key;
        this.timezone = timezone;
        client = new OkHttpClient.Builder()
                .readTimeout(1, TimeUnit.MINUTES).build();
        serializer = new Gson();
    }

    public EventResults getEvents(
            final LocalDateTime start,
            final LocalDateTime end) throws ClientException {
        return getEvents("all", start, end);

    }

    private EventResults getEvents(
            final String eventType, final LocalDateTime start,
            final LocalDateTime end) throws ClientException {

        final String url = baseUrl + String.format(
                           TEMPLATE_URL, eventType,
                           start.toEpochSecond(timezone),
                           end.toEpochSecond(timezone));

        final Request request = new Request.Builder().addHeader(KEY_HEADER, key)
                .url(url).build();

        try {
            final Response response = client.newCall(request).execute();

            final String responseString = response.body().string();

            final EventResults result = serializer.fromJson(
                    responseString, EventResults.class);

            return result;
        } catch (final IOException e) {
            throw new ClientException(e);
        }

    }
}
