/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import java.util.List;
import lombok.Value;

/**
 *
 * @author Public Transit Analytics
 */
@Value
public class PercentileResults {
    private final List<PercentileResult> result;
}
