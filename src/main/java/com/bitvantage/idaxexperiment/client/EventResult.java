/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author Public Transit Analytics
 */
@RequiredArgsConstructor
public class EventResult {

    private final String segment;
    private final int startTime;
    private final int endTime;
    private final String value;
    private final String value_descriptor;
    private final String vendor;
}
