/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.client;

import lombok.Value;

/**
 *
 * @author Public Transit Analytics
 */
@Value
public class PercentileResult {
    private final String name;
    private final String route_id;
    private final int datetime;
    private final float value;
    private final int num_records; 
}
