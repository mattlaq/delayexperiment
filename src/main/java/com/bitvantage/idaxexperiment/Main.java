/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment;

import com.bitvantage.idaxexperiment.client.BaselineClient;
import com.bitvantage.idaxexperiment.client.RouteClient;
import com.bitvantage.idaxexperiment.client.StandardReportingClient;
import com.bitvantage.idaxexperiment.service.BaselineTable;
import com.bitvantage.idaxexperiment.service.CalculationException;
import com.bitvantage.idaxexperiment.service.Correlator;
import com.bitvantage.idaxexperiment.service.DataReporter;
import com.bitvantage.idaxexperiment.service.DelayCalculator;
import com.bitvantage.idaxexperiment.service.DelayVisualizer;
import com.bitvantage.idaxexperiment.service.Mapper;
import com.bitvantage.idaxexperiment.service.RelativeDelayMeasurement;
import com.bitvantage.idaxexperiment.service.Route;
import com.bitvantage.idaxexperiment.service.RouteException;
import com.bitvantage.idaxexperiment.service.RoutesProvider;
import com.bitvantage.idaxexperiment.service.VisualizerException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Public Transit Analytics
 */
@Slf4j
public class Main {

    private static final String BASE_URL
            = "https://q6s4e02cd7.execute-api.us-west-2.amazonaws.com/test";
    private static final String EVENT_BASE_URL
            = "https://81go0cerq3.execute-api.us-west-2.amazonaws.com/test";

    private static final String API_KEY
            = "";
    private static final ZoneOffset ZONE_OFFSET
            = OffsetDateTime.now().getOffset();

    public static void main(final String[] args) {

        try {
            final StandardReportingClient reportingClient
                    = new StandardReportingClient(
                            BASE_URL, API_KEY);

            final BaselineClient baselineClient
                    = new BaselineClient(BASE_URL, API_KEY);

            final RouteClient routeClient = new RouteClient(BASE_URL, API_KEY);
            final RouteAdaptor routeAdaptor = new RouteAdaptor();

            final BaselineTable baselineTable
                    = new BaselineTable(baselineClient);
            final DataReporter dataReporter
                    = new DataReporter(reportingClient, 5);
            final RoutesProvider routesProvider
                    = new RoutesProvider(routeClient, routeAdaptor);

            final Set<Route> measuredRoutes = routesProvider.getAllRoutes();
            final Correlator correlator = new Correlator();
            correlator.addRoutes(measuredRoutes);

            final DelayCalculator delayCalculator = new DelayCalculator(
                    baselineTable, dataReporter, correlator);

            final OffsetDateTime beginTime
                    = LocalDateTime.of(2018, Month.DECEMBER, 5, 7, 0).atOffset(
                            ZONE_OFFSET);
            final OffsetDateTime endTime
                    = LocalDateTime.of(2018, Month.DECEMBER, 5, 9, 0).atOffset(
                            ZONE_OFFSET);

            final Map<String, RelativeDelayMeasurement> calculation
                    = delayCalculator.getRelativeSegmentDelays(
                            beginTime, endTime);

            final DelayVisualizer visualizer = new DelayVisualizer(
                    correlator, new Mapper());
            visualizer.visualizeDelay(calculation);

        } catch (final VisualizerException | CalculationException
                               | RouteException e) {
            log.error("Expected Exception.", e);
        } catch (final Exception e) {
            log.error("Unexpected Exception.", e);
        }

    }

}
