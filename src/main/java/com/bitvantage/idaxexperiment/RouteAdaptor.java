/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment;

import com.bitvantage.idaxexperiment.client.RouteResult;
import com.bitvantage.idaxexperiment.service.Coordinate;
import com.bitvantage.idaxexperiment.service.Route;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Public Transit Analytics
 */
@Slf4j
public class RouteAdaptor {

    private static final Pattern TYPE_PATTERN = Pattern.compile(
            "type=(\\w+)");
    private static final Pattern COORDINATES_PATTERN
            = Pattern.compile("coordinates=\\[(.+)\\]");
    private static final Pattern COORDINATE_PATTERN
            = Pattern.compile("(.+), (.+)");
    private static final String LINE_STRING_TYPE = "LineString";
    private static final String POINT_TYPE = "Point";

    public Optional<Route> getRoute(final RouteResult result) {

        final String id = result.getRoute_id();
        final String name = result.getName();

        final String segmentsString = result.getShared_streets_segs();
        final List<String> segments = Arrays.stream(
                segmentsString.replaceAll("\\[", "").replaceAll("\\]", "")
                        .split(",")).map(String::trim)
                .collect(Collectors.toList());

        final String geometryString = result.getGeometry().replace("{", "")
                .replace("}", "");
        final String[] keys = geometryString.split(",", 2);
        final String typeKeyValue = keys[0].trim();

        final Matcher typeMatcher = TYPE_PATTERN.matcher(typeKeyValue);
        typeMatcher.find();
        final String type = typeMatcher.group(1);

        final String coordinatesKeyValue = keys[1].trim();

        final Matcher coordinatesMatcher
                = COORDINATES_PATTERN.matcher(coordinatesKeyValue);
        if (!coordinatesMatcher.find()) {
            log.warn("No coordinates for {}", result);
            return Optional.empty();
        }

        if (LINE_STRING_TYPE.equals(type)) {
            final String coordinatesString = coordinatesMatcher.group(1);

            final String[] sections = coordinatesString.split("],");
            final List<Coordinate> coordinates = Stream.of(sections)
                    .map(s -> s.replaceAll("\\[", ""))
                    .map(s -> s.replaceAll("]", "")).map(String::trim)
                    .map(s -> mapCoordinate(s))
                    .collect(Collectors.toList());
            return Optional.of(new Route(id, name, coordinates, segments));
        } else if (POINT_TYPE.equals(type)) {
            final String coordinateString = coordinatesMatcher.group(1)
                    .replace("\\[", "").replace("]", "");
            final Coordinate coordinate = mapCoordinate(coordinateString);

            return Optional.of(new Route(
                    id, name, Collections.singletonList(coordinate), segments));
        } else {
            return Optional.empty();
        }
    }

    private static Coordinate mapCoordinate(
            final String coordinateString) {
        final Matcher matcher = COORDINATE_PATTERN.matcher(coordinateString);
        matcher.find();
        return new Coordinate(Double.valueOf(matcher.group(2)),
                              Double.valueOf(matcher.group(1)));

    }

}
