/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

/**
 *
 * @author Public Transit Analytics
 */
public class RouteException extends Exception {
    
    public RouteException(final Exception e) {
        super(e);
    }
    
}
