/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import java.util.Iterator;
import java.util.List;
import lombok.Value;

/**
 *
 * @author Public Transit Analytics
 */
@Value
public class Route {

    private final String routeId;
    private final String name;
    private final List<Coordinate> coordinates;
    private final List<String> segments;

    public double getLength() {
        double accumulator = 0;
        final Iterator<Coordinate> iter = coordinates.iterator();

        if (iter.hasNext()) {
            Coordinate start = iter.next();

            while (iter.hasNext()) {
                final Coordinate end = iter.next();
                accumulator += Math.sqrt(
                        Math.pow(start.getLatitude() - end.getLatitude(), 2) +
                        Math.pow(start.getLongitude() - end.getLongitude(), 2));
                start = end;
            }
        }
        return accumulator;
    }
}
