/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.bitvantage.idaxexperiment.client.ClientException;

/**
 *
 * @author Public Transit Analytics
 */
public class BaselineException extends Exception {

    public BaselineException(final ClientException e) {
        super(e);
    }
    
}
