/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Multimap;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Public Transit Analytics
 */
@RequiredArgsConstructor
public class DelayCalculator {

    private final BaselineTable baseline;
    private final DataReporter data;
    private final Correlator correlator;

    public Map<String, RelativeDelayMeasurement> getRelativeSegmentDelays(
            final OffsetDateTime start, final OffsetDateTime end)
            throws CalculationException {
        try {

            ImmutableMap.Builder<String, RelativeDelayMeasurement> builder
                    = ImmutableMap.builder();
            for (final String seg : correlator.getSegements()) {
                final Collection<Route> routes = correlator.getRoutesForSegment(
                        seg);

                final Multimap<OffsetDateTime, Double> allVolumes
                        = HashMultimap.create();
                final Multimap<OffsetDateTime, Double> allFactors
                        = HashMultimap.create();

                for (final Route route : routes) {
                    final Map<OffsetDateTime, Double> volumeData
                            = data.getVolumeMeasurements(start, end, route);

                    volumeData.entrySet().forEach(e -> allVolumes.put(
                            e.getKey(), e.getValue()));

                    final Map<OffsetDateTime, Double> timeData
                            = data.getTimeMeasurements(start, end, route);

                    if (!timeData.isEmpty()) {
                        for (final OffsetDateTime sampleTime
                                     : timeData.keySet()) {
                            final double time = timeData.get(sampleTime);

                            if (time != 0) {
                                final List<Double> baselineTimes
                                        = baseline.getTravelTime(
                                                route.getRouteId(),
                                                sampleTime);
                                final double averageBaseline = baselineTimes
                                        .stream().collect(
                                                Collectors.averagingDouble(d
                                                        -> d));
                                final double factor
                                        = (time - averageBaseline) /
                                          averageBaseline;
                                allFactors.put(sampleTime, factor);
                            }
                        }
                    }
                }
                final RelativeDelayMeasurement measurement
                        = new RelativeDelayMeasurement(
                                allVolumes.values().stream()
                                        .collect(Collectors.averagingDouble(
                                                d -> d)),
                                ImmutableSortedSet.copyOf(allFactors.values()));
                builder.put(seg, measurement);
            }
            return builder.build();
        } catch (BaselineException e) {
            throw new CalculationException(e);
        }
    }

}
