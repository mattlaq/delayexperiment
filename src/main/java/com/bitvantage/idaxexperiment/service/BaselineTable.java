/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.bitvantage.idaxexperiment.client.BaselineClient;
import com.bitvantage.idaxexperiment.client.TimeBaselineResult;
import com.bitvantage.idaxexperiment.client.ClientException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

/**
 *
 * @author Public Transit Analytics
 */
public class BaselineTable {

    private final BaselineClient client;
    private final ListMultimap<RouteHour, Double> table;

    public BaselineTable(final BaselineClient client) {
        this.client = client;
        table = ArrayListMultimap.create();
    }

    public List<Double> getTravelTime(
            final String routeId, final OffsetDateTime requestedTime) 
            throws BaselineException {
        if (table.isEmpty()) {
            try {
                final List<TimeBaselineResult> travelTimes 
                        = client.getTravelTimes();
                for (final TimeBaselineResult travelTime : travelTimes) {
                    final String id = travelTime.getId();
                    final int epoch = travelTime.getPeriod();
                    final OffsetDateTime time = Instant.ofEpochSecond(epoch)
                            .atOffset(ZoneOffset.UTC);
                    final double total = travelTime.getTravel_time();
                    table.put(new RouteHour(time.getHour(), id), total);
                }

            } catch (final ClientException e) {
                throw new BaselineException(e);
            }
        }

        final int hour = requestedTime.withOffsetSameInstant(ZoneOffset.UTC)
                .getHour();
        return table.get(new RouteHour(hour, routeId));
    }

}
