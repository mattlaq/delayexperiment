/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.bitvantage.idaxexperiment.client.ClientException;
import com.bitvantage.idaxexperiment.client.ReportingResult;
import com.bitvantage.idaxexperiment.client.ReportingResults;
import com.bitvantage.idaxexperiment.client.StandardReportingClient;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.Table;
import com.google.common.collect.TreeRangeSet;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Public Transit Analytics
 */
public class DataReporter {

    private final StandardReportingClient client;
    private final Table<String, OffsetDateTime, Double> timeTable;
    private final Table<String, OffsetDateTime, Double> volumeTable;
    private final RangeSet<LocalDateTime> cachedTimes;
    private final RangeSet<LocalDateTime> cachedVolumes;
    private final int interval;

    public DataReporter(final StandardReportingClient client,
                        final int interval) {
        this.client = client;
        volumeTable = HashBasedTable.create();
        timeTable = HashBasedTable.create();
        cachedVolumes = TreeRangeSet.create();
        cachedTimes = TreeRangeSet.create();
        this.interval = interval;
    }

    public Map<OffsetDateTime, Double> getVolumeMeasurements(
            final OffsetDateTime start, final OffsetDateTime end,
            final Route route)
            throws CalculationException {

        final LocalDateTime utcStart = start.withOffsetSameInstant(
                ZoneOffset.UTC).toLocalDateTime();
        final LocalDateTime utcEnd = end.withOffsetSameInstant(ZoneOffset.UTC)
                .toLocalDateTime();

        if (!cachedVolumes.encloses(Range.closed(utcStart, utcEnd))) {
            try {
                final ReportingResults results = client
                        .getVolume(interval, utcStart, utcEnd);
                populateTable(results.getResult(), volumeTable);
                cachedVolumes.add(Range.closed(utcStart, utcEnd));
            } catch (final ClientException e) {
                throw new CalculationException(e);
            }

        }
        return volumeTable.row(route.getRouteId());
    }

    public Map<OffsetDateTime, Double> getTimeMeasurements(
            final OffsetDateTime start, final OffsetDateTime end,
            final Route route)
            throws CalculationException {

        final LocalDateTime utcStart = start.withOffsetSameInstant(
                ZoneOffset.UTC).toLocalDateTime();
        final LocalDateTime utcEnd = end.withOffsetSameInstant(ZoneOffset.UTC)
                .toLocalDateTime();

        if (!cachedTimes.encloses(Range.closed(utcStart, utcEnd))) {
            try {
                final ReportingResults results = client
                        .getTravelTimes(interval, utcStart, utcEnd);
                populateTable(results.getResult(), timeTable);
                cachedTimes.add(Range.closed(utcStart, utcEnd));
            } catch (final ClientException e) {
                throw new CalculationException(e);
            }

        }
        return timeTable.row(route.getRouteId());
    }

    private void populateTable(
            final List<ReportingResult> results, final Table table) {
        for (final ReportingResult result : results) {
            final String routeId = result.getRoute_id();
            final OffsetDateTime time = OffsetDateTime.ofInstant(
                    Instant.ofEpochSecond(result.getDatetime()), ZoneOffset.UTC);
            final double volume = result.getValue();
            table.put(routeId, time, volume);
        }
    }
}
