/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author Public Transit Analytics
 */
public class Correlator {

    private final Multimap<String, Route> segToRoute;
    private final Multimap<Route, String> routeToSeg;

    public Correlator() {
        segToRoute = HashMultimap.create();
        routeToSeg = HashMultimap.create();
    }
    
    public void addRoutes(final Collection<Route> routes) {
        for (final Route route : routes) {
            for (final String segment : route.getSegments()) {
                segToRoute.put(segment, route);
                routeToSeg.put(route, segment);
            }
        }
    }
    
    public Set<String> getSegements() {
        return segToRoute.keySet();
    }
    
    public Collection<Route> getRoutesForSegment(final String segment) {
        return segToRoute.get(segment);
    }

}
