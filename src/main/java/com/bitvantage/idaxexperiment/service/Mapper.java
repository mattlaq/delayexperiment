/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.google.common.collect.ListMultimap;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.awt.color.ColorSpace;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Iterator;
import java.util.List;
import org.jfree.graphics2d.svg.SVGGraphics2D;

/**
 *
 * @author Public Transit Analytics
 */
public class Mapper {

    private static final double WIDTH = 1000;
    private static final double HEIGHT = 1000;

    private static final double POINT_SIZE = 2.0;

    private static final double NORTH_POINT = 47.734135;
    private static final double SOUTH_POINT = 47.481003;
    private static final double NS_DELTA = NORTH_POINT - SOUTH_POINT;
    private static final double EAST_POINT = -122.459694;
    private static final double WEST_POINT = -122.224434;
    private static final double EW_DELTA = WEST_POINT - EAST_POINT;

    public String createMap(final ListMultimap<Route, Double> instructions) {
        final SVGGraphics2D svgGenerator = createDocument();
        for (final Route route : instructions.keySet()) {
            markCoordinates(route.getName(), route.getCoordinates(),
                            route.getLength(), instructions.get(route),
                            svgGenerator);
        }

        return svgGenerator.getSVGDocument();
    }

    private void markCoordinates(
            final String name, final List<Coordinate> coordinates,
            final double totalLength, List<Double> relativeAmount,
            final SVGGraphics2D svgGenerator) {

        int amount = 0;
        double accumulatedLength = 0;
        double switchThreshold = totalLength / relativeAmount.size();

        final float colors[] = {(float) Math.random(),
            (float) Math.random(),
            (float) Math.random()};
        svgGenerator.setPaint(new Color(
                ColorSpace.getInstance(ColorSpace.CS_sRGB), colors, 0.5F));
        
        svgGenerator.setFont(svgGenerator.getFont().deriveFont(1.0F));

        if (coordinates.isEmpty()) {
        } else if (coordinates.size() == 1) {
            final Coordinate coordinate = coordinates.get(0);
            svgGenerator.drawString(name, (float) getXDelta(coordinate),
                                    (float) getYDelta(coordinate));
            final Shape point = getPoint(coordinate);

            svgGenerator.fill(point);
        } else {
            svgGenerator.setStroke(new BasicStroke(5F * relativeAmount
                .get(amount).floatValue()));
            
            final Iterator<Coordinate> iter = coordinates.iterator();

            Coordinate start = iter.next();
            svgGenerator.drawString(name, (float) getXDelta(start),
                                    (float) getYDelta(start));

            while (iter.hasNext()) {
                final Coordinate end = iter.next();
                final Shape line = getLine(start, end);
                svgGenerator.draw(line);

                final double length = getSegmentLength(start, end);
                accumulatedLength += length;
                if (accumulatedLength > switchThreshold) {
                    amount++;
                    final float stroke = 5F * relativeAmount
                            .get(amount).floatValue();
                    svgGenerator.setStroke(new BasicStroke(stroke));
                    accumulatedLength = 0;
                }

                start = end;
            }

        }
    }

    private static SVGGraphics2D createDocument() {

        final SVGGraphics2D svgGenerator = new SVGGraphics2D((int) WIDTH,
                                                             (int) HEIGHT);
        return svgGenerator;
    }

    private static double getSegmentLength(final Coordinate point1,
                                           final Coordinate point2) {
        return Math.sqrt(
                Math.pow(point1.getLatitude() - point2.getLatitude(), 2) +
                Math.pow(point1.getLongitude() - point2.getLongitude(), 2));

    }

    private static Shape getLine(final Coordinate point1,
                                 final Coordinate point2) {

        final double x1 = getXDelta(point1);
        final double y1 = getYDelta(point1);

        final double x2 = getXDelta(point2);
        final double y2 = getYDelta(point2);
        final Line2D line = new Line2D.Double(x1, y1, x2, y2);
        return line;
    }

    private static Shape getPoint(final Coordinate point) {
        final double offset = POINT_SIZE / 2;

        final double y = getYDelta(point);
        final double x = getXDelta(point);

        return new Ellipse2D.Double(x + offset, y + offset,
                                    POINT_SIZE, POINT_SIZE);
    }

    private static double getXDelta(final Coordinate point) {
        return WIDTH * ((point.getLongitude() -
                         EAST_POINT) / EW_DELTA);
    }

    private static double getYDelta(final Coordinate point) {
        return HEIGHT * ((NORTH_POINT -
                          point.getLatitude()) / NS_DELTA);

    }

}
