/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import lombok.Value;

/**
 *
 * @author Public Transit Analytics
 */
@Value
public class Coordinate {
    private final double latitude;
    private final double longitude; 
}
