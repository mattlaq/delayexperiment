/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.google.common.collect.ImmutableListMultimap;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Public Transit Analytics
 */
@RequiredArgsConstructor
public class DelayVisualizer {

    private final Correlator correlator;
    private final Mapper mapper;

    public void visualizeDelay(
            final Map<String, RelativeDelayMeasurement> results)
            throws VisualizerException {
        try {
            final ImmutableListMultimap.Builder<Route, Double> builder
                    = ImmutableListMultimap.builder();
            for (final Map.Entry<String, RelativeDelayMeasurement> entry
                         : results.entrySet()) {
                final Collection<Route> routes = correlator.getRoutesForSegment(
                        entry.getKey());
                final RelativeDelayMeasurement measurement = entry.getValue();

                final NavigableSet<Double> factors = measurement.getFactors();
                final List<Double> relativeAmounts;
                if (factors.isEmpty()) {
                    relativeAmounts = Collections.emptyList();
                } else {
                    final double min = factors.first();
                    final double max = factors.last();
                    relativeAmounts = factors.stream()
                            .map(d -> (d - min) / (max - min))
                            .collect(Collectors.toList());
                }

                for (final Route route : routes) {
                    builder.putAll(route, relativeAmounts);
                }

            }

            final String map = mapper.createMap(builder.build());

            final FileWriter writer = new FileWriter("visualization.svg");
            writer.write(map);
            writer.close();

        } catch (final IOException e) {
            throw new VisualizerException(e);
        }

    }

}
