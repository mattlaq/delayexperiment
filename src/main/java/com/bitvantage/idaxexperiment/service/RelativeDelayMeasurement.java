/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import java.util.NavigableSet;
import lombok.Value;

/**
 *
 * @author Public Transit Analytics
 */
@Value
public class RelativeDelayMeasurement {
   private final double averageVolume;
   private final NavigableSet<Double> factors;
}
