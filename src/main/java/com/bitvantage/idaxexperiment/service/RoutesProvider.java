/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitvantage.idaxexperiment.service;

import com.bitvantage.idaxexperiment.RouteAdaptor;
import com.bitvantage.idaxexperiment.client.ClientException;
import com.bitvantage.idaxexperiment.client.RouteClient;
import com.bitvantage.idaxexperiment.client.RouteResults;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Public Transit Analytics
 */
@RequiredArgsConstructor
public class RoutesProvider {

    private final RouteClient routeClient;
    private final RouteAdaptor routeAdaptor;

    public Set<Route> getAllRoutes() throws RouteException {
        try {
        final RouteResults descriptions = routeClient.getAllDescriptions();
        final Set<Route> routes = descriptions.getResult().stream()
                .map(routeAdaptor::getRoute)
                .filter(route -> route.isPresent())
                .map(optional -> optional.get())
                //.filter(route -> route.getName().startsWith("*CBD"))
                .collect(Collectors.toSet());
        return routes;
        } catch (final ClientException e) {
            throw new RouteException(e);
        }
    }
}
